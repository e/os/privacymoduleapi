package foundation.e.privacymodules.trackers

/**
 * Get reporting about trackers calls.
 */
interface ITrackTrackersPrivacyModule {

    fun start(trackers: List<Tracker>, enableNotification: Boolean = true)

    /**
     * List all the trackers encountered for a specific app.
     */
    fun getTrackersForApp(appUid: Int): List<Tracker>

    /**
     * Return the number of encountered trackers since "ever"
     */
    fun getTrackersCount(): Int

    /**
     * Return the number of encountere trackers since "ever", for each app uid.
     */
    fun getTrackersCountByApp(): Map<Int, Int>

    /**
     * Return the number of encountered trackers for the last 24 hours
     */
    fun getPastDayTrackersCount(): Int

    /**
     * Return the number of encountered trackers for the last month
     */
    fun getPastMonthTrackersCount(): Int

    /**
     * Return the number of encountered trackers for the last year
     */
    fun getPastYearTrackersCount(): Int


    /**
     * Return number of trackers calls by hours, for the last 24hours.
     * @return list of 24 numbers of trackers calls by hours
     */
    fun getPastDayTrackersCalls(): List<Pair<Int, Int>>

    /**
     * Return number of trackers calls by day, for the last 30 days.
     * @return list of 30 numbers of trackers calls by day
     */
    fun getPastMonthTrackersCalls(): List<Pair<Int, Int>>

    /**
     * Return number of trackers calls by month, for the last 12 month.
     * @return list of 12 numbers of trackers calls by month
     */
    fun getPastYearTrackersCalls(): List<Pair<Int, Int>>

    fun getPastDayTrackersCallsByApps(): Map<Int, Pair<Int, Int>>

    fun getPastDayTrackersCallsForApp(appUId: Int): Pair<Int, Int>

    fun getPastDayMostLeakedApp(): Int

    interface Listener {

        /**
         * Called when a new tracker attempt is logged. Consumer may choose to call other methods
         * to refresh the data.
         */
        fun onNewData()
    }

    fun addListener(listener: Listener)

    fun removeListener(listener: Listener)

    fun clearListeners()
}