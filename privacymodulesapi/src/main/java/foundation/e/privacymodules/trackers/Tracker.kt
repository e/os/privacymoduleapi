package foundation.e.privacymodules.trackers

/**
 * Describe a tracker.
 */
data class Tracker(
    val id: String,
    val hostnames: Set<String>,
    val label: String,
    val exodusId: String?
)
